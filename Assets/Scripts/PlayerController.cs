﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    float speed = 7f;
    float jumpForce = 10f;
    float move;
    public Rigidbody2D rb;
    Animator animator;
    public AudioSource audioSource;
    public Transform groundCheck;
    public LayerMask groundLayer;

    public AudioClip jumpClip, hurtClip, coinClip;

    public GameObject died;

    float hurtTime = 0.5f;
    float currentTime = 0f;
    float nextTime = 0f;

    bool isGrounded = false;
    public bool canClimb = false;
    public Warning warnSign;
    public bool hasCollided = false;
    public bool canMove = true;

    public Text cherryPoint; 
    public Text lifePoint;   

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        GameManager.Instance.StartCounting();
        UpdateUI();
    }

    void Update()
    {
        Movement();
        UpdateAnimation();
    }

    void FixedUpdate()
    {
        currentTime += Time.deltaTime;
        if (hasCollided)
        {
            hasCollided = false;
        }
        move = Input.GetAxis("Horizontal");
    }

    private void Movement()
    {
        if (!canMove)
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            return;
        }
        float vertical = Input.GetAxis("Vertical");
        rb.velocity = new Vector2(speed * move, rb.velocity.y);
        if (move != 0)
        {
            transform.localScale = new Vector3(Mathf.Sign(move) * 4, 4, 4);
        }
        animator.SetBool("Running", Mathf.Abs(move) > 0.1f);
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            audioSource.PlayOneShot(jumpClip, 1f);
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            if (Mathf.Abs(move) > 0f)
            {
                animator.SetBool("Running", false);
                animator.SetBool("JumpUp", true);
            }
        }
        if (canClimb && Mathf.Abs(vertical) > 0.1f)
        {
            rb.gravityScale = 0f;
            rb.velocity = new Vector2(0f, vertical * speed);
            animator.SetBool("Climbing", true);
        }
        else
        {
            rb.gravityScale = 2f;
            animator.SetBool("Climbing", false);
        }
    }

    private void UpdateAnimation()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
        animator.SetBool("JumpUp", !isGrounded && rb.velocity.y > 0);
        animator.SetBool("FallingDown", !isGrounded);
        animator.SetBool("Running", Mathf.Abs(move) > 0f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            TakeDam();
            if (rb.velocity.y < 0)
            {
                audioSource.PlayOneShot(jumpClip, 1f);
                rb.velocity = new Vector2(rb.velocity.x, jumpForce * 0.5f);
                Destroy(collision.gameObject);
            }
            else
            {
                GameManager.Instance.TakeDamage();
                audioSource.PlayOneShot(hurtClip, 1f);
                if (collision.transform.position.x >= transform.position.x)
                {
                    if (currentTime >= nextTime)
                    {
                        nextTime += hurtTime;
                        rb.velocity = new Vector2(-4f * speed * move, jumpForce * 0.5f);
                        animator.SetTrigger("Hurt");
                    }
                }
                else
                {
                    if (currentTime >= nextTime)
                    {
                        nextTime += hurtTime;
                        rb.velocity = new Vector2(4f * speed * move, jumpForce * 0.5f);
                        animator.SetTrigger("Hurt");
                    }
                }
            }
        }
        else if (collision.CompareTag("Heart"))
        {
            Destroy(collision.gameObject);
            GameManager.Instance.AddLife();
            audioSource.PlayOneShot(coinClip, 1f);
            hasCollided = true;
        }
        else if (collision.CompareTag("Cherry"))
        {
            Destroy(collision.gameObject);
            GameManager.Instance.AddCherry();
            audioSource.PlayOneShot(coinClip, 1f);
            hasCollided = true;
        }

        if (collision.CompareTag("Ladder"))
        {
            canClimb = true;
        }

        if (collision.CompareTag("DeadZone"))
        {
            TakeDam();
            GameManager.Instance.TakeDamage();
            Respawn();
        }
        UpdateUI();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Ladder"))
        {
            canClimb = false;
        }
    }

    public void TakeDam()
    {
        if (GameManager.Instance.Life <=1)
        {
            Death();
        }
    }

    public void Death()
    {
        GameManager.Instance.StopBackgroundMusic();
        audioSource.Stop();
        GameManager.Instance.ResetScoresAndTime();
        Time.timeScale = 0;
        died.SetActive(true);
    }

    public void RestartGame()
    {
        GameManager.Instance.ResetScoresAndTime();
        GameManager.Instance.ResetPlayer();
        GameManager.Instance.StartCounting();
        UpdateUI();
        died.SetActive(false);
        Time.timeScale = 1;
        GameManager.Instance.PlayBackgroundMusic();
    }

    public void Respawn()
    {
        transform.position = GameManager.Instance.GetSavepoint();
        rb.velocity = Vector2.zero; 
    }

    private void UpdateUI()
    {
        lifePoint.text = GameManager.Instance.Life.ToString();
        cherryPoint.text = GameManager.Instance.Cherry.ToString();
    }
}
