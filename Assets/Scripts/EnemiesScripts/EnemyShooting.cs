using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public GameObject bullet;
    public Transform bulletPos;
    private float timer;
    private GameObject player;
    public bool shoot = false;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > 2 && shoot == true)
        {
            timer = 0;
            shooting();
        }
    }
    public void shooting()
    {
        Instantiate(bullet, bulletPos.position, Quaternion.identity);
    }
}
