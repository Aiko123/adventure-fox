﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeMove : MonoBehaviour
{
    public Transform player; 
    public float chaseSpeed = 2f; 
    public float returnSpeed = 1f; 
    public float chaseRange = 5f; 

    private Animator animator;
    private Vector3 originalPosition; 
    private bool isChasing = false; 

    private void Start()
    {
        animator = GetComponent<Animator>();
        originalPosition = transform.position;
    }

    private void Update()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);

        if (distanceToPlayer <= chaseRange)
        {
            isChasing = true;
            animator.SetBool("Chasing", true); 

            Vector3 direction = (player.position - transform.position).normalized;

            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity))
            {
                Vector3 surfaceNormal = hit.normal;
                direction = Vector3.ProjectOnPlane(direction, surfaceNormal).normalized;
            }

            transform.position += direction * chaseSpeed * Time.deltaTime;

            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, direction.y, 0));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
        }
        else
        {
            if (transform.position != originalPosition)
            {
                Vector3 returnDirection = (originalPosition - transform.position).normalized;

                transform.position += returnDirection * returnSpeed * Time.deltaTime;

                if (Vector3.Distance(transform.position, originalPosition) < 0.1f)
                {
                    animator.SetBool("Chasing", false);
                }

                Quaternion returnLookRotation = Quaternion.LookRotation(new Vector3(returnDirection.x, returnDirection.y, 0));
                transform.rotation = Quaternion.Slerp(transform.rotation, returnLookRotation, Time.deltaTime * 5f);
            }
            else
            {
                isChasing = false;
            }
        }
    }
}
