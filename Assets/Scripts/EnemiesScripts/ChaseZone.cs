﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseZone : MonoBehaviour
{
    private BatChasing batChasing;

    void Start()
    {
        batChasing = GetComponentInParent<BatChasing>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            batChasing.OnTriggerEnter2D(collision);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            batChasing.OnTriggerExit2D(collision);
        }
    }
}
