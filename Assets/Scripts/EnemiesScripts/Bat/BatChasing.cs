﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatChasing : MonoBehaviour
{
    public float chaseSpeed = 5f;
    public float returnSpeed = 3f;
    private Animator animator;
    public GameObject chaseZone;

    private Vector3 originalPosition;
    private Transform playerTransform;
    private bool isChasing = false;

    void Start()
    {
        originalPosition = transform.position;

        // Khởi tạo animator
        animator = GetComponent<Animator>();
        if (animator == null)
        {
            Debug.LogError("Animator component not found on " + gameObject.name);
        }

        if (chaseZone != null && chaseZone.GetComponent<Collider2D>() != null)
        {
            chaseZone.GetComponent<Collider2D>().isTrigger = true;
        }
    }

    void Update()
    {
        if (isChasing)
        {
            ChasePlayer();
        }
        else
        {
            ReturnToOriginalPosition();
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            isChasing = true;
            playerTransform = collision.transform;

            // Kiểm tra animator trước khi sử dụng
            if (animator != null)
            {
                animator.SetBool("BatChasing", true);
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            isChasing = false;
        }
    }

    private void ChasePlayer()
    {
        if (playerTransform != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, playerTransform.position, chaseSpeed * Time.deltaTime);
            Vector3 direction = playerTransform.position - transform.position;
            if (direction.x >= 0.1f)
            {
                transform.localScale = new Vector3(2, 2, 1);
            }
            else if (direction.x <= -0.1f)
            {
                transform.localScale = new Vector3(-2, 2, 1);
            }
        }
    }

    private void ReturnToOriginalPosition()
    {
        transform.position = Vector3.MoveTowards(transform.position, originalPosition, returnSpeed * Time.deltaTime);
        Vector3 direction = originalPosition - transform.position;
        if (direction.x >= 0.1f)
        {
            transform.localScale = new Vector3(2, 2, 1);
        }
        else if (direction.x <= -0.1f)
        {
            transform.localScale = new Vector3(-2, 2, 1);
        }

        if (Vector3.Distance(transform.position, originalPosition) < 0.01f)
        {
            // Kiểm tra animator trước khi sử dụng
            if (animator != null)
            {
                animator.SetBool("BatChasing", false);
            }
        }
    }
}
