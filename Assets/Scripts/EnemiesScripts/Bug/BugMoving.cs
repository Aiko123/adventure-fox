﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugMoving : MonoBehaviour
{
    public float speed;
    private GameObject player;
    public bool chaseBug = false;
    public Transform startingPosition;
    public Animator animator;
    public bool isReturning = false;
    public float delay = 3f;
    public float patrolRange = 5f;
    public float chaseRange = 10f; // Khoảng cách bắt đầu đuổi theo
    public float shootRange = 3f; // Khoảng cách bắn
    public GameObject bulletPrefab; // Prefab đạn
    public float bulletSpeed = 10f;
    public float fireRate = 1f; // Tốc độ bắn
    private float nextFireTime = 0f;

    private Vector3 patrolTarget;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        SetPatrolTarget();
    }

    void Update()
    {
        if (player == null)
        {
            return;
        }

        float distanceToPlayer = Vector2.Distance(transform.position, player.transform.position);

        if (distanceToPlayer <= chaseRange)
        {
            chaseBug = true;
        }
        else
        {
            chaseBug = false;
        }

        if (chaseBug)
        {
            ChasePlayer();
        }
        else
        {
            Patrol();
        }
    }

    private void SetPatrolTarget()
    {
        patrolTarget = startingPosition.position + new Vector3(Random.Range(-patrolRange, patrolRange), Random.Range(-patrolRange, patrolRange), 0);
    }

    private void Patrol()
    {
        transform.position = Vector2.MoveTowards(transform.position, patrolTarget, speed * Time.deltaTime);
        Flip(patrolTarget);

        if (Vector2.Distance(transform.position, patrolTarget) < 0.1f)
        {
            SetPatrolTarget();
        }

        if (!chaseBug && isReturning)
        {
            ReturnToStart();
        }
    }

    private void ChasePlayer()
    {
        float distanceToPlayer = Vector2.Distance(transform.position, player.transform.position);

        if (distanceToPlayer > shootRange)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
        }
        else if (distanceToPlayer <= shootRange && distanceToPlayer > 2f)
        {
            if (Time.time >= nextFireTime)
            {
                Shoot();
                nextFireTime = Time.time + 1f / fireRate;
            }
        }
        else if (distanceToPlayer <= 2f)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, -speed * Time.deltaTime);
        }

        Flip(player.transform.position);
    }

    private void ReturnToStart()
    {
        transform.position = Vector2.MoveTowards(transform.position, startingPosition.position, speed * Time.deltaTime);
        Flip(startingPosition.position);

        if (Vector2.Distance(transform.position, startingPosition.position) < 0.1f)
        {
            isReturning = false;
            SetPatrolTarget();
        }
    }

    private void Flip(Vector3 target)
    {
        if (transform.position.x > target.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }

    private void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();

        Vector2 direction = (player.transform.position - transform.position).normalized;
        rb.velocity = direction * bulletSpeed;

        // Flip the bullet to face the correct direction
        if (transform.rotation.eulerAngles.y == 0)
        {
            bullet.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            bullet.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }
}
