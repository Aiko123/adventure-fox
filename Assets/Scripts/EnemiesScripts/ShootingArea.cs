using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingArea : MonoBehaviour
{
    public EnemyShooting[] enemyArray;
    
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            foreach (EnemyShooting enemy in enemyArray)
            {
                enemy.shoot = true;
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collison)
    {
        if (collison.CompareTag("Player"))
        {
            foreach (EnemyShooting enemy in enemyArray)
            {
                enemy.shoot = false;
            }
        }
    }
}
