using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsScripts : MonoBehaviour
{
    public int heal;
    public int manaheal;
    public bool enter;
    public AudioClip alarm;
    private AudioSource source;
    public Collection collection;
    void Start()
    {
        enter = false;
        source = GetComponent<AudioSource>();
    }
    void Update()
    {
        if (enter == true)
        {
            source.PlayOneShot(alarm); 
            enter = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collection.updateText();
            Destroy(gameObject);
        }
    }
}
