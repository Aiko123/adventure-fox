﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePoint : MonoBehaviour
{
    public float fadeDuration = 1f;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            LeanTween.alpha(gameObject, 0f, fadeDuration).setEase(LeanTweenType.easeOutQuad).setOnComplete(() =>
            {
                Destroy(gameObject);
            });
            GameManager.Instance.SetSavepoint(transform.position);
        }
    }
}
