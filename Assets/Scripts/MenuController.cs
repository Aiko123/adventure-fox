﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [SerializeField] GameObject pauseMenu;
    AudioSource audioSource;
    public string levelName;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
        if (audioSource != null && audioSource.isPlaying)
        {
            audioSource.Pause();
        }
        GameManager.Instance.StopBackgroundMusic(); 
    }

    public void NewGame()
    {
        GameManager.Instance.ResetScoresAndTime();
        SceneManager.LoadScene("TheForest");
        Time.timeScale = 1;
        GameManager.Instance.PlayBackgroundMusic();
    }

    public void Quit()
    {
        SceneManager.LoadScene("StartScene");
        Time.timeScale = 1;
        GameManager.Instance.ResetScoresAndTime();
    }

    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
        if (audioSource != null)
        {
            audioSource.UnPause();
        }
        GameManager.Instance.ResumeBackgroundMusic();
    }
}
