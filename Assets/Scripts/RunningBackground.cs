using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningBackground : MonoBehaviour
{
    public float scrollSpeed = 1f;

    private void FixedUpdate()
    {
        Vector2 pos = transform.position;
        pos.x -=Time.deltaTime;
        if (pos.x < -39.32)
        {
            pos.x = 0;
        }
        transform.position = pos;
    }
}
