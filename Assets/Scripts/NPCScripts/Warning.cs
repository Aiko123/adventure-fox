﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static Unity.Burst.Intrinsics.X86.Avx;

public class Warning : MonoBehaviour
{
    public GameObject warnSign;
    public TextMeshProUGUI textMeshPro;
    public string[] lines;
    private int index = 0;
    public float textSpeed = 0.1f;
    private bool isTyping = false;
    public bool isDialogueActive = false;
    private PlayerController playerController; // Tham chiếu tới script của Player

    void Start()
    {
        textMeshPro.text = string.Empty;
        warnSign.SetActive(false); // Ban đầu ẩn dialogue

        // Tìm đối tượng Player và lấy script điều khiển di chuyển của nó
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            playerController = player.GetComponent < PlayerController>();
        }
    }
    void Update()
    {
        if (isTyping == false && warnSign.activeSelf && Input.GetMouseButtonDown(0))
        {
            NextLine();
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && !isDialogueActive)
        {
            warnSign.SetActive(true);
            StartDialogue();
            // Khóa di chuyển của Player
            if (playerController != null)
            {
                playerController.canMove = false;
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            warnSign.SetActive(false);
            index = 0;
        }
    }

    void StartDialogue()
    {
        if (lines.Length > 0)
        {
            isDialogueActive = true;
            StartCoroutine(TypeLine());
        }
    }

    IEnumerator TypeLine()
    {
        isTyping = true;
        foreach (char c in lines[index].ToCharArray())
        {
            textMeshPro.text += c;
            yield return new WaitForSeconds(textSpeed);
        }
        isTyping = false;
    }

    void NextLine()
    {
        if (index < lines.Length - 1)
        {
            index++;
            textMeshPro.text = string.Empty;
            StartCoroutine(TypeLine());
        }
        else
        {
            warnSign.SetActive(false);
            isDialogueActive = false;
            // Mở khóa di chuyển của Player
            if (playerController != null)
            {
                playerController.canMove = true;
            }
        }
    }
}
