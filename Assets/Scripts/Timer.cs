using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timeText;

    private void Update()
    {
        UpdateTimeText();
    }

    private void UpdateTimeText()
    {
        if (GameManager.Instance != null)
        {
            float gameTime = GameManager.Instance.GameTime;
            int minutes = Mathf.FloorToInt(gameTime / 60f);
            int seconds = Mathf.FloorToInt(gameTime % 60f);
            string timeString = string.Format("{0:00}:{1:00}", minutes, seconds);
            timeText.text = "Time: " + timeString;
        }
    }
}
