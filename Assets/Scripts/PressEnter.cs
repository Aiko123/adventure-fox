using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class PressEnter : MonoBehaviour
{
    public Image pressEnter;
    private bool isFlashing = false;

    void Start()
    {
        StartCoroutine(FlashImage());
    }
    IEnumerator FlashImage()
    {
        while (true)
        {
            isFlashing = !isFlashing;
            pressEnter.color = isFlashing ? Color.white : Color.clear;
            yield return new WaitForSeconds(0.5f);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene("TheHole"); 
        }
    }
}
