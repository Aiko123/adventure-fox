﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public AudioSource audioSource;
    private float gameTime = 0f;
    public float GameTime => gameTime;

    private bool isCounting = false;

    public int Life { get; private set; }
    public int Cherry { get; private set; }
    private Vector3 savepoint;
    private Vector3 initialPlayerPosition;
    private Vector3 initialSavepointPosition;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            audioSource.loop = true;
            audioSource.playOnAwake = false;
            audioSource.Play();
            LoadScores();
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (isCounting)
        {
            gameTime += Time.deltaTime;
        }
    }

    public Vector3 GetSavepoint()
    {
        return savepoint;
    }

    public void StartCounting()
    {
        isCounting = true;
    }

    public void StopCounting()
    {
        isCounting = false;
    }

    public void ResetGameTime()
    {
        gameTime = 0f;
    }

    public void StopBackgroundMusic()
    {
        if (audioSource != null)
        {
            audioSource.Pause();
        }
    }

    public void ResumeBackgroundMusic()
    {
        if (audioSource != null && !audioSource.isPlaying)
        {
            audioSource.UnPause();
        }
    }

    public void PlayBackgroundMusic()
    {
        if (audioSource != null)
        {
            audioSource.Stop();
            audioSource.Play();
        }
    }
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "TheForest")
        {
            PlayBackgroundMusic();
        }
    }

    public void AddLife()
    {
        Life = Mathf.Min(Life+1, 5);
        SaveScores();
    }

    public void AddCherry()
    {
        Cherry += 1;
        SaveScores();
    }

    public void TakeDamage()
    {
        Life -= 1;
        if (Life < 0)
        {
            Life = 0;
        }
        SaveScores();
    }

    public void SaveScores()
    {
        PlayerPrefs.SetInt("Life", Life);
        PlayerPrefs.SetInt("Cherry", Cherry);
        PlayerPrefs.Save();
    }

    private void LoadScores()
    {
        Life = PlayerPrefs.GetInt("Life", 5);
        Cherry = PlayerPrefs.GetInt("Cherry", 0);
    }

    public void SetSavepoint(Vector3 position)
    {
        savepoint = position;
        Debug.Log("Savepoint set at: " + savepoint);
    }

    public void ResetScoresAndTime()
    {
        Life = 5;
        Cherry = 0;
        ResetGameTime();
        SaveScores();
    }

    public void ResetPlayer()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            player.transform.position = initialPlayerPosition;
        }
        savepoint = initialSavepointPosition;
        Life = 5;
        Cherry = 0;
        SaveScores();
        initialPlayerPosition = player.transform.position;
        initialSavepointPosition = savepoint;
    }
    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded; // Unregister the callback when the object is destroyed
    }
}
